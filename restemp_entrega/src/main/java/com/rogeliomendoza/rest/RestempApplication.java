package com.rogeliomendoza.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestempApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestempApplication.class, args);
	}

}

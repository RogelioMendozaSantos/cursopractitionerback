package com.rogeliomendoza.productos.productos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping(path = "/apitechu/v2")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel productoModel) {
        return productoService.save(productoModel);
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel productoModel){
        productoService.save(productoModel);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel prod){
        return productoService.delete(prod);
    }
}

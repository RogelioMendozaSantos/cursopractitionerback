package com.rogeliomendoza.productos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepoprodutosapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RepoprodutosapiApplication.class, args);
	}

}
